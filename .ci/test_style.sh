#!/bin/bash

pip install --upgrade pip setuptools
pip install -r requirements.txt
pip install pylint flake8
flake8                           ukbparse --exclude=ukbparse/tests || true
pylint --output-format=colorized ukbparse --ignore=ukbparse/tests  || true
