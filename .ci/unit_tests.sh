#!/bin/bash

set -e

apt-get update -y
apt-get install -y bsdmainutils

pip install --upgrade pip setuptools
pip install -r requirements.txt
pip install -r requirements-demo.txt
pip install -r requirements-test.txt
pip freeze
pytest
